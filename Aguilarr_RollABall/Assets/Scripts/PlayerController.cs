﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    // Create public varibles that can be viewed or change through the Unity Editor 
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    // Calls on the particle effect assest 
    public GameObject pickupEffect;
    // Calls on the trapdoor object that block the players way.
    public GameObject trapdoor1;
    public GameObject trapdoor2; 

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;
    //Calls forth the material of the player 
    private Material mymat;
    
   



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
        // Gets the renderer components of the player ready 
        mymat = GetComponent<Renderer>().material;
    
    
    }
    // movement funtion 
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }
    // Funtion scale the player up by .1 in all directions
    void OnGrow ()
    {
        {
            transform.localScale += new Vector3(.1f, .1f, .1f);
        }
    }
    // Shrink the players by .1 in all axis 
    void OnShrink()
    {
        {
            transform.localScale -= new Vector3(.1f, .1f, .1f);
        }
    }
    void SetCountText()
    {

    // sets score to relative count of picked items, sets up a win varible and text
        countText.text = "Count: " + count.ToString();
        if(count >= 36)
        {
            winTextObject.SetActive(true);
        }
    }



    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);

        // Changes the emission of the object once it reaches a certian score
        // Destroys the Trapdoor to let player through
        if (count >= 12)
        {
            mymat.SetColor("_EmissionColor", Color.red);
            mymat.EnableKeyword("_EMISSION");
            Destroy(trapdoor1);
        }


     
        
        if (count >= 24)
        {
            mymat.SetColor("_EmissionColor", Color.green);
            mymat.EnableKeyword("_EMISSION");
            Destroy(trapdoor2);
        }


        if (count >= 36)
        {
            mymat.SetColor("_EmissionColor", Color.cyan);
            mymat.EnableKeyword("_EMISSION");
        }

    }
    // Funtion to allow the pick up item to be interacted with 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {

            // setting score based on everytime the item is picked up 
            other.gameObject.SetActive(false);
            // Upon picking up the item the particle effect in activated and produced from the PickUp item
            pickupEffect.SetActive(true);
            Instantiate(pickupEffect, other.transform.position, other.transform.rotation);

            count = count + 1;

            SetCountText();
        }


    }

}
